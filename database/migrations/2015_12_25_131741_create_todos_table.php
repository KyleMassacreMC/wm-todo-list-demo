<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            //Create an auto incrementing column called id
            $table->increments('id');
            //Create an unsigned integer column called user_id to reference the owner of this list
            $table->integer('user_id');
            //Create a name for this todo list for visual reasons
            $table->string('name');
            //Create some timestamps "Updated At" and "Created At"
            $table->timestamps();
            //Create soft deletes. This isnt required but oh well
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Drop the table we migrated
        Schema::drop('todos');
    }
}
